package ventana;

import java.awt.event.KeyEvent;
import javax.swing.*;

public class Cronometro extends javax.swing.JFrame {
    
    boolean suspendido = false;
    int seg, min, hora, deci, con;
    
    public Cronometro() {
        
        initComponents();
        seg = 0;
        min = 0;
        hora = 0;
        deci = 0;
        con = 1;
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setTitle("CRONÓMETRO");
        btnParar.setVisible(false);
        btnIniciar.setVisible(false);
        btnreiniciar.setVisible(false);
        jButton2.setVisible(false);
        jButton1.setVisible(false);
        
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbltiempo = new javax.swing.JLabel();
        btnIniciar = new javax.swing.JButton();
        btnParar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        btnreiniciar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtTiempos = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbltiempo.setFont(new java.awt.Font("Tahoma", 1, 73)); // NOI18N
        lbltiempo.setText("00:00:00:00");
        getContentPane().add(lbltiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, -1));

        btnIniciar.setText("INICIAR / REANUDAR");
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });
        getContentPane().add(btnIniciar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, -1, -1));

        btnParar.setText("PARAR");
        btnParar.setEnabled(false);
        btnParar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPararActionPerformed(evt);
            }
        });
        btnParar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPararKeyPressed(evt);
            }
        });
        getContentPane().add(btnParar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));

        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 270, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Exit [Esc]");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 280, -1, -1));

        jButton2.setText("Scrambles");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 270, -1, -1));

        btnreiniciar.setText("REINICIAR");
        btnreiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnreiniciarActionPerformed(evt);
            }
        });
        getContentPane().add(btnreiniciar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Parar     [Space]");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon("C:\\Users\\usuario\\Documents\\NetBeansProjects\\Prueba\\src\\main\\java\\ventana\\rubikia.png")); // NOI18N
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Iniciar   [Hold Enter]");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 160, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("Scrambles [S]");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 250, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("Reiniciar  [R]");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 220, -1, -1));

        txtTiempos.setEditable(false);
        txtTiempos.setColumns(20);
        txtTiempos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTiempos.setRows(5);
        txtTiempos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTiemposKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTiemposKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(txtTiempos);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 0, 0));

        jLabel3.setIcon(new javax.swing.ImageIcon("C:\\Users\\usuario\\Documents\\NetBeansProjects\\Prueba\\src\\main\\java\\ventana\\fondo rubikia (1).jpeg")); // NOI18N
        jLabel3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel3KeyPressed(evt);
            }
        });
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 540, 310));

        pack();
    }// </editor-fold>//GEN-END:initComponents
/**/
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        System.exit(0);
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed
    
    private void txtTiemposKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTiemposKeyPressed
        
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            
            btnParar.setEnabled(false);
            hilo.suspend();
            suspendido = true;
            String tiempo = txtTiempos.getText();
            String nuevo = con + ".     " + lbltiempo.getText();
            txtTiempos.setText(nuevo + "\n" + tiempo);
            con++;
        }        
        if (evt.getKeyCode() == KeyEvent.VK_R) {
            
            hilo.suspend();
            suspendido = true;
            seg = 0;
            min = 0;
            hora = 0;
            deci = 0;
            con = 1;
            lbltiempo.setText("00:00:00:00");
            txtTiempos.setText("");
        }
        if (evt.getKeyCode() == KeyEvent.VK_S) {
            
            Inicio atras = new Inicio();
            atras.setVisible(true);
            this.setVisible(false);
        }
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }

// TODO add your handling code here:
    }//GEN-LAST:event_txtTiemposKeyPressed
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        
        Inicio atras = new Inicio();
        atras.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton2ActionPerformed
    
    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed

        // TODO add your handling code here:
    }//GEN-LAST:event_formKeyPressed
    
    private void btnreiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnreiniciarActionPerformed
        Scrambledos ins = new Scrambledos();
        hilo.suspend();
        suspendido = true;
        seg = 0;
        min = 0;
        hora = 0;
        deci = 0;
        con = 1;
        lbltiempo.setText("00:00:00:00");
        txtTiempos.setText("");
    }//GEN-LAST:event_btnreiniciarActionPerformed
        
    private void btnPararKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPararKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPararKeyPressed
    
    private void btnPararActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPararActionPerformed
        
        btnParar.setEnabled(false);
        hilo.suspend();
        suspendido = true;
        String tiempo = txtTiempos.getText();
        String nuevo = con + ".     " + lbltiempo.getText();
        txtTiempos.setText(nuevo + "\n" + tiempo);
        con++;
    }//GEN-LAST:event_btnPararActionPerformed
    
    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        btnParar.setEnabled(true);
        if (!suspendido) {
            hilo.start();
            suspendido = true;
        } else {
            hilo.resume();
        }
    }//GEN-LAST:event_btnIniciarActionPerformed
    
    private void txtTiemposKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTiemposKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            btnParar.setEnabled(true);
            if (!suspendido) {
                hilo.start();
                suspendido = true;
            } else {
                hilo.resume();
            }
            
        }
    }//GEN-LAST:event_txtTiemposKeyReleased
    
    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        ImageIcon icon = new ImageIcon("src/logo.png");
        setIconImage(icon.getImage());

        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowActivated

    private void jLabel3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel3KeyPressed
        // TODO add your handling code here:

    }//GEN-LAST:event_jLabel3KeyPressed
    
    Thread hilo = new Thread() {
        public void run() {
            try {
                while (true) {
                    if (deci > 99) {
                        deci = 0;
                        seg++;
                    }
                    if (seg > 59) {
                        seg = 0;
                        min++;
                    }
                    if (min > 59) {
                        min = 0;
                        hora++;
                    }
                    String s = "", m = "", h = "", d = "";
                    if (seg < 10) {
                        s = "0" + seg;
                    } else {
                        s = "" + seg;
                    }
                    if (min < 10) {
                        m = "0" + min;
                    } else {
                        m = "" + min;
                    }
                    if (hora < 10) {
                        h = "0" + hora;
                    } else {
                        h = "" + hora;
                    }
                    if (deci < 10) {
                        d = "0" + deci;
                    } else {
                        d = "" + deci;
                    }
                    deci++;
                    hilo.sleep(9);
                    lbltiempo.setText(h + ":" + m + ":" + s + ":" + d);
                }
            } catch (InterruptedException ie) {
                JOptionPane.showMessageDialog(rootPane, ie.getMessage());
            }
        }
    };
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Cronometro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Cronometro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Cronometro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cronometro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cronometro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciar;
    private javax.swing.JButton btnParar;
    private javax.swing.JButton btnreiniciar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbltiempo;
    private javax.swing.JTextArea txtTiempos;
    // End of variables declaration//GEN-END:variables
}
